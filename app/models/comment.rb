class Comment < ActiveRecord::Base
	has_one :post
	belongs_to :post


	validates_presence_of :post_id
	validates_presence_of :body
end
